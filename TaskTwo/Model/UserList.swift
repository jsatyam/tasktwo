//
//  UserList.swift
//  TaskTwo
//
//  Created by Jyotiraditya Satyam on 24/01/22.
//

import Foundation

class UserListResponse: Decodable {
    let page: Int
    let total: Int
    let perPage: Int
    let totalPages: Int
    let data: [UserList]
    
    enum CodingKeys: String, CodingKey {
        case page, total, data
        case perPage = "per_page"
        case totalPages = "total_pages"
    }
}

class UserList: Decodable {
    let id: Int
    let email, firstName, lastName: String
    let avatar: String
    
    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar
    }
}
