//
//  UserListVM.swift
//  TaskTwo
//
//  Created by Jyotiraditya Satyam on 24/01/22.
//

import Foundation

class UserListVM {
    
    // MARK: - Instance properties
    private var limit = 10
    private var currentPage = 1
    var userList: [UserList] = []
    var isNextPageAvailable = true
    var isInitialFetchCompleted = false
    
    // MARK: - Helper methods
    func resetPagination() {
        limit = 10
        userList = []
        currentPage = 1
        isInitialFetchCompleted = false
        isNextPageAvailable = true
    }
    
}

// MARK: - API calling methods
extension UserListVM {
    func getUserList(completion: @escaping (Bool, String) -> Void) {
        guard let url = URL(string: "https://reqres.in/api/users?page=\(currentPage)&per_page=\(limit)") else { return }
        
        if !isInitialFetchCompleted {
            HudView.show()
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            HudView.hide()
            guard let self = self else { return }
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription)
                }
                return
            }
            
            if let urlResponse = response as? HTTPURLResponse {
                let statusCode = urlResponse.statusCode
                if statusCode != 200 {
                    DispatchQueue.main.async {
                        completion(false, "Status code \(statusCode)")
                    }
                    return
                }
            }
            
            if let data = data, let userListResponse = try? JSONDecoder().decode(UserListResponse.self, from: data) {
                if userListResponse.data.isEmpty {
                    self.userList.removeAll()
                    return
                }
                
                self.userList.append(contentsOf: userListResponse.data)
                
                let totalPage = userListResponse.totalPages
                self.isNextPageAvailable = self.currentPage < totalPage ? true : false
                
                if !self.isInitialFetchCompleted {
                    self.isInitialFetchCompleted = true
                }
                
                self.currentPage += 1
                DispatchQueue.main.async {
                    completion(true, "")
                }
            }
        }
        dataTask.resume()
    }
}
