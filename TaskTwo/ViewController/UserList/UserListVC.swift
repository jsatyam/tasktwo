//
//  UserListVC.swift
//  TaskTwo
//
//  Created by Jyotiraditya Satyam on 24/01/22.
//

import UIKit

class UserListVC: UITableViewController {
    
    // MARK: - Instance properties
    private let userListVM = UserListVM()
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        userListVM.resetPagination()
        getUserList()
    }
    
}

// MARK: - Table view data source
extension UserListVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        userListVM.userList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let data = userListVM.userList[indexPath.row]
        cell.textLabel?.text = data.firstName + data.lastName
        cell.detailTextLabel?.text = data.email
        return cell
    }
}

// MARK: - Table view delegate
extension UserListVC {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = userListVM.userList.count - 1
        if indexPath.row == lastItem {
            if userListVM.isNextPageAvailable {
                tableView.showLoaderAtBottom(true)
                getUserList()
            }
        }
    }
}

// MARK: - API calling methods
extension UserListVC {
    private func getUserList() {
        userListVM.getUserList { [weak self] (success, message) in
            guard let self = self else { return }
            
            if self.userListVM.isInitialFetchCompleted {
                self.tableView.showLoaderAtBottom(false)
            }
            
            if success {
                self.tableView.reloadData()
                return
            }
            
            self.showAlert(title: "Alert", message: message)
        }
    }
}
