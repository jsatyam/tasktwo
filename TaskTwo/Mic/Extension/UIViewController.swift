//
//  UIViewController.swift
//  TaskTwo
//
//  Created by Jyotiraditya Satyam on 24/01/22.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
