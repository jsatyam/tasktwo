//
//  ViewController.swift
//  TaskTwo
//
//  Created by Jyotiraditya Satyam on 24/01/22.
//

import UIKit

class HudView: UIView {
    
    static var activityView: UIActivityIndicatorView?
    static let view = UIApplication.shared.windows.filter { $0.isKeyWindow }.first
    
    static func show() {
        guard let view = self.view else { return }
        HudView.activityView = UIActivityIndicatorView(style: .large)
        HudView.activityView?.center = view.center
        view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    static func hide() {
        if (activityView != nil){
            DispatchQueue.main.async {
                self.activityView?.stopAnimating()
                self.activityView?.removeFromSuperview()
            }
        }
    }
}
